﻿Shader "Custom/vertexColor2"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex1 ("Albedo (RGB)", 2D) = "white" {}
        _MainTex2 ("Albedo (RGB)", 2D) = "white" {}
        _MainTex3 ("Albedo (RGB)", 2D) = "white" {}

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        CGPROGRAM

        #pragma surface surf Standard noambient

        sampler2D _MainTex1;
        sampler2D _MainTex2;
        sampler2D _MainTex3;

        struct Input
        {
            float2 uv_MainTex1;
            float2 uv_MainTex2;
            float2 uv_MainTex3;
            float4 color:COLOR;
        };

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 r = tex2D (_MainTex1, IN.uv_MainTex1);
            fixed4 g = tex2D (_MainTex2, IN.uv_MainTex2);
            fixed4 b = tex2D (_MainTex3, IN.uv_MainTex3);
            o.Emission = lerp(IN.color.rgb, r.rgb, IN.color.r);
            o.Emission = lerp(o.Emission, g.rgb, IN.color.g);
            o.Emission = lerp(o.Emission, b.rgb, IN.color.b);    
            o.Alpha = IN.color.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
