﻿Shader "Custom/13"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }

        CGPROGRAM

        #pragma surface surf Lambert noambient alpha : fade
        
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 viewDir;
            float3 worldPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            float rim = saturate(dot(o.Normal, IN.viewDir));
            rim = (pow(1-rim,3) + pow(frac(IN.worldPos.g + 30 -_Time.y),30)) * abs(sin(_Time.y * 10));
            o.Alpha = rim;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
